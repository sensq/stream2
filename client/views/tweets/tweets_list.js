Meteor.subscribe("tweets");

Template.tweetsList.helpers({
	tweets: function() {
		return Tweets.find({}, {sort: {date: -1}});
	},
    tweetscount: function() {
        return Tweets.find().count();
    }
});